---
nom: La Permaculturelle
logo: /images/permaculturelle_logo.png
photo: /images/La_Permaculturelle_1.jpg
location: Le long de la Saône
---
<!-- logo -->
![La Permaculturelle](/images/permaculturelle_logo.png)
<!-- sous-titre -->
Un coworking éco-citoyen qui s’inspire des principes et éthiques de la Permaculture

# La Permaculturelle
![](/images/La_Permaculturelle_2.jpg)

Entre Saône et forêt
----
**La Permaculturelle est située le long de la Saône**, à la croisée du 4e arrondissement, du neuvième, des Monts d'Or et de Caluire-et-Cuire.

**Le coworking** s’étend sur deux niveaux, sans vis-à-vis, et **offre des espaces variés** donnant sur la rivière ou le jardin : une grande cour intérieure et son préau, une terrasse abritée des arbres, un vaste parking à flanc de falaise, la Saône omniprésente…

La force du lieu, c’est de proposer **un cadre “au vert”** à moins d’un quart d’heure à vélo de la place des Terreaux, et à seulement 800 mètres du tunnel de la Croix-Rousse.

Un projet engagé
----
La Permaculturelle est **un laboratoire de perma-entreprise**. Ici, nous testons de nouvelles formes d'organisation au travail, dans un esprit constructif et bienveillant. Ici, **nous prônons l'éco-responsabilité au bureau** en nous inspirant du modèle de la nature.

Avec 3 objectifs
----

1. Être **un lieu d'épanouissement professionnel** : on y vient pour travailler dans de bonnes conditions, on prend soin des hommes et des femmes qui viennent y travailler.

3. Être **un lieu « facilitateur » de la transition écologique**. Tout est fait pour minimiser l'impact sur l’environnement de nos adhérents et leur permettre de se reconnecter à la nature, même en milieu urbain.

5. Être **un lieu de travail, mais aussi de partage et d’échange**. On y vient pour faire grandir ses projets, on en sort grandi. On y vient pour cultiver et se cultiver.

Et de nombreux services
----
- Des postes "résidents" et des formules nomades "à la carte"
- Des intérieurs éco-rénovés et aménagements de bureau ergonomiques
- Des cabines calls/visios à chaque étage 📞 💻
- 1 parking gratuit autos/motos/vélos 🚗 🛵 🚲
- Le prêt de vélos 🚲
- L'eau filtrée, les boissons bio ☕ et les produits du jardin 🪴
- 1 cuisine équipée / 1 kitchenette / 1 salle commune
- Une douche 🚿 et un vestiaire 🏃🏻‍♀️
- Le vrac, le zéro déchet, le tri, le compostage, le ménage écologique...
- Ainsi qu'un espace extérieur de plus de 1000 m² dédié à des expérimentations de permaculture urbaine 🥦 et à l'organisation d'événements 🥂

Nous situer et nous contacter
----

📬 La Permaculturelle, c'est au **65 quai Joseph Gillet 69004 Lyon**


📝 Vous souhaitez **découvrir nos nombreuses offres** ? Venez visiter notre coworking ou faire un essai, en écrivant à cowork@lapermaculturelle.fr
📝 Plus d'infos sur [www.lapermaculturelle.fr](https://www.lapermaculturelle.fr)
