---
nom: BOOSTER HOUSE
logo: https://i.postimg.cc/fRVB33hs/LOGO-ROND-BH-removebg-preview.png
photo: https://i.postimg.cc/tgcbkRKg/Le-Salon.jpg
location: Dardilly
---

<!-- logo -->
![BOOSTER HOUSE](https://i.postimg.cc/fRVB33hs/LOGO-ROND-BH-removebg-preview.png)
<!-- sous-titre -->
Un espace de travail pour vous et près de chez vous !

# Booster House

<!-- Photo -->
![](https://i.postimg.cc/tgcbkRKg/Le-Salon.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
Intégrez un espace de Coworking dynamique et confortable où productivité et convivialité sont intimement liés afin de booster votre activité !

Cet espace est installé au cœur de la Techlid, zone économique dynamique de l’Ouest Lyonnais, dans un pôle d’entreprises totalement rénové qui vous proposera une multitude de services : Brasserie, Micro-Crèche, Conciergerie, places de parking…

Profitez de la luminosité du deuxième et dernier étage totalement réservé à votre nouvel espace !
350 m² de plateau, aménagés avec des **bureaux fermés**, des **postes en coworking** et des **salles de réunion**. Bénéficiez de nombreux **services** pour vous offrir un maximum de confort et de flexibilité au quotidien.

Intégrez notre **communauté bienveillante** de membres passionnés et bénéficiez des opportunités de réseautage et de synergies créatives.

Découvrez [nos offres](https://lyoncoworking.fr/nos-offres-tarifs/) et visitez nos espaces dès maintenant !

## Services

- Alcôves de travail pour s’isoler ou échanger
- Domiciliation d'entreprise, permanence téléphonique, secrétariat
- Espace de convivialité avec café, thé à volonté ☕️
- WIFI Fibre
- Copie, scan, impressions sécurisées *(en sus)*
- Evénéments, ateliers, soirées réseaux
- Parking dédié et gratuit 🚗

## Localisation

Situé au cœur de la Techlid, au sein du MiniParc 2.0.
Il est accessible en voiture (sortie 34 - M6) ou par transport en commun (bus n°89 - Porte Lyon/Gare de Vaise et le bus n°GE4 - Tour de Salvagny/Techlid).

## Plus d'infos

[lyoncoworking.fr](https://lyoncoworking.fr/)

Pour nous contacter : boosterhouse69@gmail.com
